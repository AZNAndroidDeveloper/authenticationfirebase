package uz.azn.auth

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import uz.azn.auth.databinding.FragmentSignUpBinding

class SignUpFragment : Fragment(R.layout.fragment_sign_up) {
    private lateinit var binding: FragmentSignUpBinding
    private lateinit var auth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSignUpBinding.bind(view)
        auth = Firebase.auth
        with(binding) {
            navigateToLogin.apply {
                setOnClickListener {
                    fragmentManager!!.beginTransaction()
                        .replace(R.id.frame_layout, LoginFragment()).commit()
                }
            }
            btnSignUp.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                if (
                    email.isNotEmpty() &&
                    password.isNotEmpty() &&
                    password.toString().length > 6
                ) {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(requireActivity()) { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(
                                    requireContext(),
                                    "Sign up successfully",
                                    Toast.LENGTH_SHORT
                                ).show()
                                fragmentManager!!.beginTransaction()
                                    .replace(R.id.frame_layout, LoginFragment()).commit()
                            } else {
                                Toast.makeText(requireContext(), "Failure", Toast.LENGTH_SHORT)
                                    .show()
                            }

                        }

                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please check your information and try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

    }

}