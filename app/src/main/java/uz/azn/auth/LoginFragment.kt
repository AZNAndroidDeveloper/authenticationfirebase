package uz.azn.auth

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import uz.azn.auth.databinding.FragmentLoginBinding


class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var auth: FirebaseAuth
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    binding = FragmentLoginBinding.bind(view)
        auth = Firebase.auth
        with(binding){
            navigateToSignUp.setOnClickListener{
                fragmentManager!!.beginTransaction().replace(R.id.frame_layout,SignUpFragment()).commit()
            }
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                if (
                    email.isNotEmpty() &&
                    password.isNotEmpty() &&
                    password.toString().length > 6
                ) {
                    auth.signInWithEmailAndPassword(email,password).addOnCompleteListener(requireActivity()){
                        task ->
                        if (task.isSuccessful){
                            fragmentManager!!.beginTransaction().replace(R.id.frame_layout,UserFragment()).commit()
                        }else{
                            Toast.makeText(
                                requireContext(),
                                "Check your email and password",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please check your information and try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
  }